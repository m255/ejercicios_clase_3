import java.util.Scanner;

public class ejercicio5 {
    public static void main(String[] args) throws Exception {
    Scanner leer = new Scanner(System.in);
    System.out.println("Digite el valor del cateto 1");
    double cat1 = leer.nextDouble();
    System.out.println("Digite el valor del cateto 2");
    double cat2 = leer.nextDouble();
    double hipotenusa = Math.sqrt(Math.pow(cat1, 2) + Math.pow(cat2, 2));
    System.out.println("La hipotenusa del triángulo es: "+hipotenusa);
    }
}
