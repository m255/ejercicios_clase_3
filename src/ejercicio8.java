import java.util.Scanner;

public class ejercicio8 {
    public static void main(String[] args) throws Exception {
    Scanner leer = new Scanner(System.in);
    System.out.println("Digite el valor del número 1");
    double num1 = leer.nextDouble();
    System.out.println("Digite el valor del número 2");
    double num2 = leer.nextDouble();
    double resultado = num1/num2; 
    if (num2 == 0){
        System.out.println("ERROR: No se puede realizar la división por cero");
    }else{
        System.out.println("El resultado es "+resultado);
    }
    }
}
