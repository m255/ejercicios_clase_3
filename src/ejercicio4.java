import java.util.Scanner;

public class ejercicio4 {
    public static void main(String[] args) throws Exception {
        Scanner leer = new Scanner(System.in);
        System.out.println("Digite la velocidad en Km/h");
        double velocidad = leer.nextDouble();
        double conversion = velocidad/3.6;
        System.out.println("La velocidad en m/s es: "+conversion+" m/s");
    }
}
