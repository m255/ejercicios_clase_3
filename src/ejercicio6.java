import java.util.Scanner;

public class ejercicio6 {
    public static void main(String[] args) throws Exception {
    Scanner leer = new Scanner(System.in);
    System.out.println("Digite el número entero");
    int num = leer.nextInt();
    if (num % 10 == 0){
        System.out.println("El número "+num+" es múltiplo de 10");
    } else{
        System.out.println("El número "+num+" no es múltiplo de 10");
    }
    }
}