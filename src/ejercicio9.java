import java.util.Scanner;
public class ejercicio9 {
    public static void main(String[] args) {

        Scanner leer = new Scanner(System.in);
        System.out.print("Introduzca primer número: ");
        int num1 = leer.nextInt();
        System.out.print("Introduzca segundo número: ");
        int num2 = leer.nextInt();
        System.out.print("Introduzca tercer número: ");
        int num3 = leer.nextInt();
        if (num1 > num2) {
            if (num1 > num3) {
                System.out.println("Número mayor " + num1);                                             
            } else {
                System.out.println("Número mayor: " + num3);     
            }
        } else if (num2 > num3) {
            System.out.println("Número mayor: " + num2);
        } else {
            System.out.println("Número mayor: " + num3);
        }
    }
}
